<?php

/**
 * Implementation of hook_views_data().
 */
function uc_views_upsell_views_data() {
  $data['uc_product_pairs']['table']['group'] = t('Product pair');
  $data['uc_product_pairs']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'pair_nid',
  );
  $data['uc_product_pairs']['nid'] = array(
    'title' => t('Node ID'),
    'help' => t('The node id.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    'relationship' => array(
      'title' => t('Product pair'),
      'help' => t('Bring in information about product pairs.'),
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Product pair'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  $data['uc_product_pairs']['pair_nid'] = array(
    'title' => t('Paired node ID'),
    'help' => t('The node id of the paired product.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  $data['uc_product_pairs']['orders'] = array(
    'title' => t('Order count'),
    'help' => t('The number of orders in which the products have been purchased together.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_data_alter().
 */
function uc_views_upsell_views_data_alter(&$data) {
  $data['node']['in_cart'] = array(
    'title' => t('Is in cart'),
    'help' => t('Filter nodes that are in the current cart.'),
    'group' => t('Product'),
    'filter' => array(
      'handler' => 'uc_views_upsell_handler_filter_cart',
      'field' => 'nid',
      'label' => t('Product is in cart'),
    ),
  );
}

/**
 * Implementation of hook_views_handlers().
 */
function uc_views_upsell_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'uc_views_upsell') . '/views',
    ),
    'handlers' => array(
      'uc_views_upsell_handler_filter_cart' => array(
        'parent' => 'views_handler_filter_boolean_operator',
      ),
    ),
  );
}
