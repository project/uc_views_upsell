<?php

/**
 * Filter nodes based on whether they are in the cart.
 */
class uc_views_upsell_handler_filter_cart extends views_handler_filter_boolean_operator {
  function query() {
    if ($items = uc_cart_get_contents()) {
      $nids = array();
      foreach ($items as $item) {
        $nids[] = $item->nid;
      }

      $this->ensure_my_table();
      $this->query->add_where($this->options['group'], "$this->table_alias.$this->real_field ". (empty($this->value) ? "NOT " : "") ."IN (". db_placeholders($nids, 'int') .")", $nids);
    }
    else {
      // Cart is empty, so filtering for "is in cart" must return nothing.
      if (!empty($this->value)) {
        $this->query->add_where($this->options['group'], "1 = 0");
      }
    }
  }
}
