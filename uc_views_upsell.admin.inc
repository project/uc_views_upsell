<?php

function uc_views_upsell_settings_form() {
  if (!$_POST) {
    $current = db_result(db_query('SELECT COUNT(DISTINCT nid) FROM {uc_product_pairs}'));
    $max = db_result(db_query('SELECT COUNT(DISTINCT n.nid) FROM {node} n INNER JOIN {uc_products} p ON n.vid = p.vid WHERE n.status = 1'));

    drupal_set_message(t('@current of @max products currently have matching pair data. Some products may not have pairs.', array('@current' => $current, '@max' => $max)));
  }

  $form['rebuild'] = array(
    '#type' => 'fieldset',
    '#title' => t('Rebuild product pairs'),
  );
  $form['rebuild']['message'] = array('#value' => '<p>' . t('Updating some of the ordered product pairs is performed on each cron run, but you can also force a full rebuild. This may take some time if you have a large number of products and/or orders in your store.') . '</p>');
  $form['rebuild']['rebuild'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild now'),
    '#submit' => array('uc_views_upsell_settings_rebuild_submit'),
  );

  $form['uc_views_upsell_cron_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of products to update on each cron run'),
    '#description' => t('Products are selected at random.'),
    '#default_value' => variable_get('uc_views_upsell_cron_limit', 100),
    '#size' => 3,
  );

  $form['uc_views_upsell_pair_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of products to store for each pair'),
    '#description' => t('As product pairs are intended to be sorted by number of orders, you only need to store the top N matching products for each product. For example, if you are showing a maximum of 8 other products and excluding products currently in the cart, you should set this to about 20 as more than this is unlikely to be needed.'),
    '#default_value' => variable_get('uc_views_upsell_pair_limit', 20),
    '#size' => 3,
  );

  return system_settings_form($form);
}

function uc_views_upsell_settings_rebuild_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/store/settings/uc_views_upsell/rebuild';
}

function uc_views_upsell_rebuild_confirm() {
  return confirm_form(array(), t('Are you sure you want to rebuild all ordered product pairs?'), 'admin/store/settings/uc_views_upsell', NULL, t('Rebuild product pairs'), t('Cancel'));
}

function uc_views_upsell_rebuild_confirm_submit($form, &$form_state) {
  $batch = array(
    'operations' => array(
      array('uc_views_upsell_rebuild_batch', array()),
    ),
    'finished' => 'uc_views_upsell_rebuild_batch_finished',
    'title' => t('Rebuilding product pairs'),
    'file' => drupal_get_path('module', 'uc_views_upsell') . '/uc_views_upsell.admin.inc',
  );
  batch_set($batch);

  batch_process('admin/store/settings/uc_views_upsell');
}

function uc_views_upsell_rebuild_batch(&$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = db_result(db_query('SELECT COUNT(DISTINCT n.nid) FROM {node} n INNER JOIN {uc_products} p ON n.vid = p.vid WHERE n.status = 1'));
  }

  $result = db_query_range("SELECT n.nid FROM {node} n INNER JOIN {uc_products} p ON n.vid = p.vid WHERE n.nid > %d AND n.status = 1 ORDER BY nid ASC", $context['sandbox']['current_node'], 0, 20);
  while ($node = db_fetch_object($result)) {
    uc_views_upsell_update_node($node->nid);

    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $node->nid;
    $context['message'] = t('Now processing product @current (@progress of @count).', array('@current' => $context['sandbox']['current_node'], '@progress' => $context['sandbox']['progress'], '@count' => $context['sandbox']['max']));
  }

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

function uc_views_upsell_rebuild_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('The product pairs were rebuilt.'));
  }
  else {
    drupal_set_message(t('The product pairs were not successfully rebuilt.'), 'error');
  }
}
